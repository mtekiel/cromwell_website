<?php
namespace App\Services;

use Symfony\Component\Validator\Constraints;
use App\Exception\ReportException;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Entity\ContentManagement;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ContentManagementRepository;
//exceptions
use App\Exception\ContentManagementException;


//Service containing all main CRUD methods for ContentManagement Entity
class ContentManagementService
{

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

    }

    public function findAll()
    {
        $contents = $this->em->getRepository(ContentManagement::class)->findAll();

        return $contents;
    }
    public function findOne($id)
    {
        $content = $this->em->getRepository(ContentManagement::class)->find($id);
        return $content;
    }
    public function delete($id)
    {
        $contents = $this->em->getRepository(ContentManagement::class)->find($id);
        $this->em->remove($contents);
        $this->em->flush();
    }
    public function create(Request $request){
        $content = new ContentManagement();
        $content->setContentKey($request->request->get('contentKey'));
        $content->setContentValue($request->request->get('contentValue'));

        $this->em->persist($content);
        $this->em->flush();

    }
    public function update (Request $request, $id){

        $content = $this->em->getRepository(ContentManagement::class)->find($id);
        $content->setContentKey($request->request->get('contentKey'));
        $content->setContentValue($request->request->get('contentValue'));

    }

}