<?php

namespace App\Exception;


use App\Exception\AbstractGenericException;
use Throwable;

class ContentManagementException extends AbstractGenericException
{
    protected $message = "ContentManagementException: ";
    //the rest is inherited
}
