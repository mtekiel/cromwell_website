<?php

namespace App\Entity;

use App\Repository\ContentManagementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContentManagementRepository::class)
 */
class ContentManagement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content_key;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentKey(): ?string
    {
        return $this->content_key;
    }

    public function setContentKey(string $content_key): self
    {
        $this->content_key = $content_key;

        return $this;
    }

    public function getContentValue(): ?string
    {
        return $this->content_value;
    }

    public function setContentValue(?string $content_value): self
    {
        $this->content_value = $content_value;

        return $this;
    }
}
