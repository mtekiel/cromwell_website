<?php

namespace App\Repository;

use App\Entity\ContentManagement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContentManagement|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContentManagement|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContentManagement[]    findAll()
 * @method ContentManagement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentManagementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContentManagement::class);
    }

    /**
     * @param $contentKey
     * @return mixed
     * Method finding particular field value in code...
     */

    public function findContentValueByKey($content_key)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s.content_value')
                ->andWhere('s.content_key = :content_key' )
                ->setParameter('content_key', $content_key)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult()
                ;

        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Error: ".$e->getMessage(), $e->getCode());
        }

    }
    // /**
    //  * @return ContentManagement[] Returns an array of ContentManagement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContentManagement
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
