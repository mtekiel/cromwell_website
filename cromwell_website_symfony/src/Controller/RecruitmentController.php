<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ContentManagement;
use App\Exception\ContentManagementException;
//services
use App\Services\ContentManagementService;

class RecruitmentController extends AbstractController
{

    private $statService;
    public function __construct(ContentManagementService $contentManagementService)
    {
        $this->contentManagementService = $contentManagementService;
    }

    /**
     * Method presents selected content on Recruitment Page. Data taken from content_management table
     * @Route("/recruitment", name="recruitment_frontend")     *
    */
    public function index()
    {
        try {


            $intro_text = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'recruitmentpage_intro_text']);
            $benefits = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'recruitmentpage_benefits']);
            $open_positions = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'recruitmentpage_open_positions']);
            $contact_details = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'recruitmentpage_contact_details']);


            return $this->render('frontend/recruitment.html.twig', [
                'page_title' => 'Recruitment',
                'open_position_header'=>"Open positions:",
                'intro_text' => $intro_text->getContentValue(),
                'benefits' => $benefits->getContentValue(),
                'open_positions' => $open_positions->getContentValue(),
                'contact_details' => $contact_details->getContentValue(),

            ]);

        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Error: ".$e->getMessage(), $e->getCode());
        }

    }





}