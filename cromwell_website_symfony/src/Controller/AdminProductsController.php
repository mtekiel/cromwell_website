<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\ProductRepository;

class AdminProductsController extends AbstractController
{
    /**
     * @Route("/admin/products", name="admin_products")
     */
    public function index(ProductRepository $productRepository)
    {
		//show a list of all products
		$products = $productRepository->findAll();
        return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
			'products' => $products
        ]);
    }


	/**
     * @Route("/admin/products/create", name="admin_products_create")
     */
    public function create()
    {
		//show form for new products
        return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
        ]);

		//enter new products in DB
		return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
        ]);
    }

	/**
     * @Route("/admin/products/read", name="admin_products_read")
     */
    public function read()
    {
		//shows details for one product
        return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
        ]);
    }

	/**
     * @Route("/admin/products/update", name="admin_products_update")
     */
    public function update()
    {
		//show form with product details
        return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
        ]);

		//update details in DB
		return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
        ]);
    }

	/**
     * @Route("/admin/products/delete", name="admin_products_delete")
     */
    public function delete()
    {
		//delete product (link would be in product list)
        return $this->render('backend/admin_products/index.html.twig', [
            'controller_name' => 'AdminProductsController',
        ]);
    }

}
