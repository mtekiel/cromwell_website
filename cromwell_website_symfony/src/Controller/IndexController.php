<?php
namespace App\Controller;

//bundles
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Exception\ContentManagementException;


class IndexController extends AbstractController{
  	/**
  	* @Route("/admin", name="admin_index", methods={"GET"})
  	*/
  	public function admin_index()
  	{


        try {

            return $this->render('backend/base.html.twig', [

            ]);
        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),$e->getMessage(), $e->getCode());
        }

  	}
    /**
  	* @Route("/", name="index", methods={"GET"})
  	*/

    public function index()
    {
        return $this->render('frontend/base.html.twig', [

        ]);
    }

}
