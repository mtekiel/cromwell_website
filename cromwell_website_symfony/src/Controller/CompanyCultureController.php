<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ContentManagement;

//services
use App\Services\ContentManagementService;

class CompanyCultureController extends AbstractController
{

    private $statService;
    public function __construct(ContentManagementService $contentManagementService)
    {
        $this->contentManagementService = $contentManagementService;
    }

    /**
     * Method shows content of Company Culture Page. Data come from content_management table of cromwell_website db
     * @Route("/culture", name="company_culture")
     *
     */
    public function index()
    {

        try {

            $cc_vision = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'company_culture_page_vision']);

            $cc_value1 = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'company_culture_page_value1']);
            $cc_value2 = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'company_culture_page_value2']);
            $cc_value3 = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'company_culture_page_value3']);
            $cc_value4 = $this->getDoctrine()->getRepository(ContentManagement::class)->findOneBy(['content_key'=>'company_culture_page_value4']);


            return $this->render('frontend/culture.html.twig', [
                'page_title' => 'Company Culture',
                'cc_visions'=>$cc_vision->getContentValue(),
                'cc_visions_header'=>"Company Vision",
                'cc_values_header' => "Core Values For Us ",
                'cc_value1' => $cc_value1->getContentValue(),
                'cc_value2' => $cc_value2->getContentValue(),
                'cc_value3' => $cc_value3->getContentValue(),
                'cc_value4' => $cc_value4->getContentValue()
            ]);


        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Error: ".$e->getMessage(), $e->getCode());
        }

    }



}