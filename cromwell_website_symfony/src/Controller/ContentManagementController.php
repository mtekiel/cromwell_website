<?php

namespace App\Controller;

use App\Entity\ContentManagement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Exception\ContentManagementException;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\ContentManagementService;


class ContentManagementController extends AbstractController
{
    /**
     * Method shows Main Content Management Page
     * @Route("/admin/", name="admin_main", methods={"GET"})
     */
    public function index()
    {

        try {

            return $this->render('backend/base.html.twig', [
                 'page_title'=>"Admin content"
            ]);

        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),$e->getMessage(), $e->getCode());
        }

    }

    /**
     * Method displays form to add a new content
     * @Route("admin/content/create", name="admin_content_create")
     */
    public function create(ContentManagementService $cmService, Request $request)
    {
        try {

            if($request->request->has('contentKey'))
            {
                $cmService->create($request);
                return $this->redirectToRoute('admin_content_create');
            }else{
                $action = "create";
                return $this->render('backend/content_management/form.html.twig', [
                    'action' => $action,
                    'page_title'=>"Create New Content"
                ]);
            }
        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Admin Content Error: ".$e->getMessage(), $e->getCode());
        }
    }

    /**
     * Method shows all existing contents
     * @Route("admin/content/read", name="admin_content_read")
     */
    public function read(ContentManagementService $cmService)
    {
        try {
                $contents = $cmService->findAll();

                $title = "All Contents";
                return $this->render('backend/content_management/index.html.twig', [
                    'page_title' => $title,
                    'contents' => $contents,
                ]);
        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Admin Content Error: ".$e->getMessage(), $e->getCode());
        }
    }


    /**
     * Method updating record with chosen id
     * @Route("/content/update/{id}", name="content_update")
     */
    public function update($id, Request $request, ContentManagementService $cmService)
    {

        $content = $cmService->findOne($id);

        if($content instanceOf ContentManagement)
        {
            if($request->request->has('contentKey'))
            {
                $em = $this->getDoctrine()->getManager();

                $content->setContentKey($request->request->get('contentKey'));
                $content->setContentValue($request->request->get('contentValue'));

                $em -> persist($content);
                $em->flush();

                return $this->redirectToRoute('admin_content_read');
            }
            else
            {
                $action = "update/".$id;
                return $this->render('backend/content_management/update_form.html.twig', [
                    'action' => $action,
                    'content' => $content,
                ]);
            }
        }
        else
        {
            return $this->redirectToRoute('admin_content_read');

        }
    }

    /**
     *Method deleting record with chosen id
     * @Route("/content/delete/{id}", name="admin_content_delete")
     */
    public function delete($id, ContentManagementService $cmService)
    {

        try {
            $content = $cmService->findOne($id);

            if($content instanceOf ContentManagement)
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($content);
                $em->flush();
            }
            return $this->redirectToRoute('view_delete_table');
        }
        catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Admin Content Error:".$e->getMessage(), $e->getCode());
        }
    }

    /**
     * Method shows table for Update action
     * @Route("/admin/content/update", name="view_update_table")
     */
    public function viewUpdate(ContentManagementService $cmService)
    {
        try {
            $contents = $cmService->findAll();

            $title = "Update Content";
            return $this->render('backend/content_management/update_content.html.twig', [
                'page_title' => $title,
                'contents' => $contents,
            ]);
        } catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Admin Content Error:".$e->getMessage(), $e->getCode());
        }
    }

    /**
     * Method shows table for Delete action
     * @Route("/admin/content/delete", name="view_delete_table")
     */
    public function viewDelete(ContentManagementService $cmService)
    {
        try {
            $contents = $cmService->findAll();

            $title = "Delete Content";
            return $this->render('backend/content_management/delete_content.html.twig', [
                'page_title' => $title,
                'contents' => $contents,
            ]);
        } catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Admin Content Error:".$e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/admin/content/{id}", name="admin_content_details")
     */
    public function viewOne($id)
    {
        try {
            $content = $this->getDoctrine()
                ->getRepository(ContentManagement::class)
                ->find($id);

            $title = "View admin content details";
            return $this->render('backend/content_management/view_one.html.twig', [
                'page_title' => $title,
                'contents' => $content,
            ]);
        } catch (\Exception $e) {
            throw new ContentManagementException($e->getCode(),"Admin Content Error:".$e->getMessage(), $e->getCode());
        }
    }
}